﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
    , labelTime(new QLabel("Date:"))
    , dateStart(new QDateEdit)
    , labelTo(new QLabel("-"))
    , dateEnd(new QDateEdit)
    , hLayMain(new QHBoxLayout)
{
    ui->setupUi(this);

    hLayMain->addWidget(labelTime);
    hLayMain->addWidget(dateStart);
    hLayMain->addWidget(labelTo);
    hLayMain->addWidget(dateEnd);
    hLayMain->addStretch();
    hLayMain->addStretch();
    setLayout(hLayMain);

    // set dateedit
    dateStart->setCalendarPopup(true);
    dateEnd->setCalendarPopup(true);
    dateStart->setDate(QDate::currentDate());
    dateEnd->setDate(QDate::currentDate());

    // set style
    QString style = "QDateEdit{border: 1px solid #FFFFFF;border-radius: 3px;}"
                    "QDateEdit:hover{border: 1px solid #C0C4CC;}"
                    "QDateEdit::drop-down{"
                    "width: 15px;image: url(:/image/image/calendar.png);}"
                    "QCalendarWidget QWidget#qt_calendar_navigationbar{"
                    "background-color: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0,stop: 0 #313947, stop: 1 #34375E);}"
                    "QCalendarWidget QToolButton{"
                    "color: white;background-color: rgba(255, 255, 255, 0);}"
                    "QCalendarWidget QToolButton#qt_calendar_prevmonth{"
                    "qproperty-icon: url(:/image/image/calendar_pre_month.png);}"
                    "QCalendarWidget QToolButton#qt_calendar_nextmonth{"
                    "qproperty-icon: url(:/image/image/calendar_next_month.png);}"
                    "QCalendarWidget QSpinBox::up-button{"
                    "image: url(:/image/image/calendar_pre_year.png);"
                    "background-color: rgba(255, 255, 255, 100);"
                    "width:15px;}"
                    "QCalendarWidget QSpinBox::up-button:hover{"
                    "background-color: rgba(215, 218, 223, 100);}"
                    "QCalendarWidget QSpinBox::down-button{"
                    "image: url(:/image/image/calendar_next_year.png);"
                    "background-color: rgba(255, 255, 255, 100);"
                    "width:15px;}"
                    "QCalendarWidget QSpinBox::down-button:hover{"
                    "background-color: rgba(215, 218, 223, 100);}";
    dateStart->setStyleSheet(style);
    dateEnd->setStyleSheet(style);
}

Widget::~Widget()
{
    delete ui;
    labelTime->deleteLater();
    dateStart->deleteLater();
    labelTo->deleteLater();
    dateEnd->deleteLater();
    hLayMain->deleteLater();
}

