﻿#include "widget.h"
#include "ui_widget.h"
#include "jlogger.h"
#include <QDebug>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
//    JLogger::Instance()->setToNet(true);
}

Widget::~Widget()
{
    JLogger::Instance()->stop();
    delete ui;
}


void Widget::on_pushButton_clicked()
{
    qDebug("this is log debug info");
    qWarning("this is warning info");
    qInfo("this is info");
//    qFatal("this is fatal info");
//    qCritical("this is critical info");
}
