QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    dxflib/dl_dxf.cpp \
    dxflib/dl_writer_ascii.cpp \
    dxfreader.cpp \
    jgraphicstextitem.cpp \
    jgraphicsview.cpp \
    main.cpp \
    widget.cpp

HEADERS += \
    dxflib/dl_attributes.h \
    dxflib/dl_codes.h \
    dxflib/dl_creationadapter.h \
    dxflib/dl_creationinterface.h \
    dxflib/dl_dxf.h \
    dxflib/dl_entities.h \
    dxflib/dl_exception.h \
    dxflib/dl_extrusion.h \
    dxflib/dl_global.h \
    dxflib/dl_writer.h \
    dxflib/dl_writer_ascii.h \
    dxfreader.h \
    jgraphicstextitem.h \
    jgraphicsview.h \
    widget.h

FORMS += \
    widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
