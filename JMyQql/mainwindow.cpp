#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    InitMySql();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::InitMySql()
{
    db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("");//mysql的地址 123.56.143.198
    db.setPort(3306);
    db.setDatabaseName("");//连接的数据库名称
    db.setUserName("");//mysql登录名
    db.setPassword("");//mysql密码
}

void MainWindow::on_pushButton_clicked()
{
    if(db.open()) {
       qDebug()<<"success!";
       QSqlQueryModel *model = new QSqlQueryModel();
       model->setQuery("select * from LifeKeeperUsers");
       ui->tableView->setModel(model);

//        QSqlQuery query("show tables from bdm257820317_db");//thinkcmf数据库里的所有表
//        while (query.next()) {
//            qDebug() << query.value(0).toString();
//        }

//        QSqlQuery query1("select * from LifeKeeperUsers");//查询表的内容
//        while (query1.next()) {
//            // LifeKeeperUsers表个字段的内容
//            qDebug() << query1.value(0).toString() << query1.value(1).toString();
//        }
        db.close();
    }else{
       qDebug()<<"failure";
    }

}
