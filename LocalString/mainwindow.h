﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMenu>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_btnSearch_clicked();

    void on_lineEditFilter_editingFinished();


    void on_lineEditDir_editingFinished();

    void on_lineEditString_editingFinished();

    void on_tableWidget_customContextMenuRequested(const QPoint &pos);

    void on_actionOpenDir_triggered();

private:
    QStringList getFilters();

    void filterFiles(const QString &folder);

    void searchString(const QString &content);

    void AddTableRow(const QString &name, int line, const QString &content);

private:
    Ui::MainWindow *ui;

    QMenu       *m_menu;

    QString     m_folder;
    bool        m_subDir;
    QStringList m_filter;
    QStringList m_files;
};
#endif // MAINWINDOW_H
